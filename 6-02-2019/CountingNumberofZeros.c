/************************************************************************************************************
    NAME                MOBILE NUMBER                   EMAIL-ID                        EMPLOYEE-ID
------------------------------------------------------------------------------------------------------------
BUSAM HEMA LATHA         833097869              hemalatha.busam@gmail.com                 10374
***********************************************************************************************************

This program is to count the number of zeros in the last after finding the factorial of a given number.

*************************************************************************************************************/

#include<stdio.h>
#include<math.h>
int main(){
	int data,numberOfZeros = 0,c = 1;
	printf("Enter data to find number of zeros\n");
	scanf("%d",&data);
	while(data>=pow(5,c)){
		numberOfZeros = numberOfZeros+(data/pow(5,c));
		c++;
	}
	printf("Number of zeros in the given data is %d\n",numberOfZeros);
}


/****************************************OUTPUT***********************************************************
 
Enter data to find number of zeros
8
Number of zeros in the given data is 1
---------------------------------------------------------------------------------------------------------
Enter data to find number of zeros
100
Number of zeros in the given data is 24
---------------------------------------------------------------------------------------------------------
Enter data to find number of zeros
167
Number of zeros in the given data is 40
*********************************************************************************************************/
