/********************************************************************************************************
 NAME                PHONE NUMBER                      EMAILID                             EMPLOYEE-ID
 -------------------------------------------------------------------------------------------------------
 B.HEMALATHA         8333097869                     hemalatha.busam@gmail.com               10374
 -------------------------------------------------------------------------------------------------------
This program is the implementation of userdefined function string copy

*********************************************************************************************************/


#include<stdio.h>
#include<stdlib.h>

//logic for the userdefined function string copy
char* userStrcpy(char *str1,char *str2){
	int i=0;
	while((*str1=*str2)!='\0'){
		str1++;
		str2++;
	}
	return str1;
}

int main(){
	int res;
	char s1[50]="how are you";
	char s2[50]="iam fine";
	printf("*******string 1 before copying*******\n");
	printf("s1=%s\n",s1);
	userStrcpy(s1,s2);
	printf("*******string 1 after copying*********\n");
	printf("s1=%s\n",s1);

}

/*********************************************OUTPUT OF THE ABOVE PROGRAM**************************************
*******string 1 before copying*******
s1=how are you
*******string 1 after copying*********
s1=iam fine
****************************************************************************************************************/
 
