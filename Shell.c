/************************************************************************************************************************
 NAME 			EMAIL-ID  			PHONE NUMBER 			EMPLOYEE-ID 
 ------------------------------------------------------------------------------------------------------------------------
 BUSAM HEMA LATHA	hemalatha.busam@gmail.com	8333097869			  10374
 ------------------------------------------------------------------------------------------------------------------------
 This program is implementation of creating user defined shell(grep is not implemented).
 ***********************************************************************************************************************/

#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
char * arguments[100];


//reading userfrom the stdin
void readComandFromUser() {
	char *string;
	int i=0;
	string=(char *)malloc(20);
	read(0,string,100);
	string[strlen(string)-1]='\0';
	// Returns first token
	char* token = strtok(string, " |");

	// Keep printing tokens while one of the
	// delimiters present in str[].
	while (token != NULL) {
		arguments[i]=token;
		printf("%s\n",arguments[i]);
		i++;
		token = strtok(NULL, " |");
	}
	arguments[i]=token;

}


int main()
{
	int ret = -1,child_exit_status;
	char *userName;
	char cwd[1024];			

	//geting user name
	getlogin_r(userName,10);
	userName[strlen(userName)]='@';
	char name[] = "myShell$ ";
	strcat(userName,name);

	while(1)
	{
		//writing our own username@shellname to the STDOUT
		write(1,userName, strlen(userName));
		readComandFromUser();
		//if entered string equals to PWD
		if(!(strcmp(arguments[0],"pwd"))) {

			getcwd(cwd, sizeof(cwd));
			write(1, cwd, strlen(cwd));
			printf("\n");
			continue;
		}
		//if entered string equals to Exit
		else if(!(strcmp(arguments[0],"exit"))) {

			exit(0);
		}
		else if(!(strcmp(arguments[0],"cd"))) {

			chdir(arguments[1]);
			getcwd(cwd, sizeof(cwd));
			write(1, cwd, strlen(cwd));
			printf("\n");
			continue;
		}


		//if entered string is ls
		ret = fork();
		if(ret == 0) {

			execvp(arguments[0], arguments);
		}
		else
		{
			wait(&child_exit_status);
		}
	}
	return 0;
}
/**************************************OUTPUT OF THE ABOVE PROGRAM**************************************
 busam@busam-Lenovo-G505:~/Desktop$ ./a.out
busam@myShell$ pwd
pwd
/home/busam/Desktop
busam@myShell$ ls -l
ls
-l
total 551236
drwxr-xr-x 2 busam busam      4096 Feb 14 12:23 13-02-2019
-rw-r--r-- 1 busam busam 483786752 Feb 14 10:00 2018-11-13-raspbian-stretch.img
-rw-r--r-- 1 busam busam  80609280 Feb 14 10:00 2018-11-13-raspbian-stretch.zip
drwxr-xr-x 2 busam busam      4096 Feb 22 17:26 22-02-2019
-rwxr-xr-x 1 busam busam     13128 Feb 22 18:22 a.out
drwxr-xr-x 7 busam busam      4096 Feb  5 13:08 Calculator
drwxr-xr-x 7 busam busam      4096 Feb  5 14:11 CalculatorFunctionalities
-rwxr-xr-x 1 busam busam       619 Feb 16 13:36 external_commands.sh
-rw-r--r-- 1 busam busam       806 Feb 22 15:09 orphan.c
drwxrwxr-x 2 busam busam      4096 Feb 21 09:19 ProgrammingTest1
-rw-r--r-- 1 busam busam      1909 Feb 22 18:22 shell.c
-rw-r--r-- 1 busam busam      1532 Feb 22 18:22 Shell.c
-rwxr-xr-x 1 busam busam      1704 Feb 16 13:36 shellTest.sh
-rw-r--r-- 1 busam busam      1030 Feb 22 16:30 shwll.c
-rw-r--r-- 1 busam busam       757 Feb 22 15:18 zombie.c
busam@myShell$ ls 
ls
13-02-2019			 22-02-2019  CalculatorFunctionalities	ProgrammingTest1  shellTest.sh
2018-11-13-raspbian-stretch.img  a.out	     external_commands.sh	shell.c		  shwll.c
2018-11-13-raspbian-stretch.zip  Calculator  orphan.c			Shell.c		  zombie.c
busam@myShell$ cd Calculator
cd
Calculator
/home/busam/Desktop/Calculator
busam@myShell$ ls
ls
bin  build  include  obj  readme  src
busam@myShell$ exit
exit
busam@busam-Lenovo-G505:~/Desktop$ 
******************************************************************************************************/

