/**********************************************************************************************************************************
NAME                         PHONE NUMBER                      EMPLOYEE ID                            EMAIL ID                            
BUSAM HEMA LATHA                8333097869                       10374                                  hemalatha.busam@gmail.com

This program is implementation of single Linked List where we adding data at specific position, deleting at specific position, updating the data at a position and displaying the data in the linked list
*************************************************************************************************************************/

#include<iostream>
#include<stdlib.h>

using namespace std;
//creating structure of type node
struct node
{
	//declaring the variables
	int data;
	node *next;//self-referential structure
}*head;
int length=0;
//creating a class of SingleLinkedList type
class SingleLinkedList{
	//declaring the pointer variable
	//struct node *head;
	public:
	//default constructor
	SingleLinkedList(int data)
	{
		head = new struct node;
		head->data=data;
		head->next=NULL;
		length++;
	}
	//member functions

	void addNodeAtAnyPosition(int,int);
	void deleteNodeAtAnyPosition(int);
	void displayLinkedListData();
	void menu();
	void update(int);
};
//method to add node at any position
void SingleLinkedList :: addNodeAtAnyPosition(int position, int data)
{
	if(position < 0 || position > length)
	{
		cout<<"cannot add at that position"<<endl;
	}else if(position == 0){
		node *newnode = new node;
		newnode->data = data;
		newnode->next = head;
		head=newnode;
		length++;
		}else if(position==length){
			node *temp;
			node *newnode = new node;
			length++;
			temp = head;
			while(temp->next!=NULL)
				temp=temp->next;
			temp->next = newnode;
			newnode->data = data;
			newnode->next = NULL;	
			}else{	
				int index = 1;
				node *newnode = new node;
				newnode->data = data;
				node *currnode = head;
				node *prevnode = head;
				while(index!=position){
					prevnode = currnode;
					currnode = currnode->next;
					index = index+1;
				}
			prevnode->next = newnode;
			newnode->next = currnode;
			length++;
			}
		}		
		
	

//displaying the linked list data
void SingleLinkedList :: displayLinkedListData()
{
 if(head == NULL){
	cout<<"List is empty"<<endl;
	}else{
		node *temp = head;
		while(temp!=NULL){
			cout<<temp->data<<endl;
			temp = temp->next;
	}
}
}

//method to update data at any postion in the linked list
void SingleLinkedList :: update(int position)
 {
              if(position<=0 || position > length) {
                        cout<<"cannot update the data at that position:"<<position<<endl;

                }
                int index;
                 node *temp=head;
                for(index=1;index<position;index++) {
                        temp=temp->next;
                }
                cout<<"enter the data to be update :"<<endl;
                cin>>temp->data;
		return;
              }

//this method is to delete data at any position in the linked list
void SingleLinkedList :: deleteNodeAtAnyPosition(int position)
{
	if(position <0 || position >length){
		cout<<"invalid length"<<endl;
	}else if(position == 0){
		node *temp=head;
		head = head->next; 
		delete temp;
		temp = NULL;
		length = length-1;
		}else{
			int count = 0;
			node *currnode = head;
			node *prevnode = head;
			while(count != position){
				prevnode = currnode;
				currnode = currnode->next;
				count++;
			}
			prevnode->next = currnode->next;
			delete currnode;
			currnode=NULL;
			length--;
		}
}

//method to call other methods and display menu
void SingleLinkedList ::  menu(){
	//declaring the variables
	int choice;
	int data;
	int position;
	while(1){
		cout<<"*************MENU******************"<<endl;
		cout<<"1.add data to the linked list "<<endl<<"2.delete data"<<endl<<"3.display the data"<<endl<<"4.updating the linkedlist"<<endl<<"5.EXIT"<<endl;
		cout<<"enter your choice";
		cin>>choice;
		switch(choice){
			case 1:cout<<"enter the data";
				cin>>data;
				cout<<"enter position";
				cin>>position;
				addNodeAtAnyPosition(position,data);
				break;
			case 2:cout<<"enter the position where data to be deleted";
				int position;
				cin>>position;
				deleteNodeAtAnyPosition(position);
				break;
			case 3:cout<<"displaying the data elements"<<endl;
				displayLinkedListData();
				break;
			case 4:cout<<"enter the postion";
				cin>>position;
				update(position);
				break;
			case 5:exit(0);
			default:cout<<"enter valid input";
		}
}
}
//driver program to run above program	
int main(){
	SingleLinkedList obj(1);//creating object of SingleLinkedList type
	obj.menu();
	return 0;
}

/***************************************OUTPUT OF THE ABOVE PROGRAM******************************************************
*************MENU******************
1.add data to the linked list 
2.delete data
3.display the data
4.updating the linkedlist
5.EXIT
enter your choice1
enter the data10
enter position1
*************MENU******************
1.add data to the linked list 
2.delete data
3.display the data
4.updating the linkedlist
5.EXIT
enter your choice1
enter the data20
enter position2
*************MENU******************
1.add data to the linked list 
2.delete data
3.display the data
4.updating the linkedlist
5.EXIT
enter your choice3
displaying the data elements
1
10
20
*************MENU******************
1.add data to the linked list 
2.delete data
3.display the data
4.updating the linkedlist
5.EXIT
enter your choice1
enter the data50
enter position5
cannot add at that position
*************MENU******************
1.add data to the linked list 
2.delete data
3.display the data
4.updating the linkedlist
5.EXIT
enter your choice3
displaying the data elements
1
10
20
*************MENU******************
1.add data to the linked list 
2.delete data
3.display the data
4.updating the linkedlist
5.EXIT
enter your choice4
enter the postion2
enter the data to be update :
20
*************MENU******************
1.add data to the linked list 
2.delete data
3.display the data
4.updating the linkedlist
5.EXIT
enter your choice3
displaying the data elements
1
20
20
*************MENU******************
1.add data to the linked list 
2.delete data
3.display the data
4.updating the linkedlist
5.EXIT
enter your choice2
enter the position where data to be deleted1
*************MENU******************
1.add data to the linked list 
2.delete data
3.display the data
4.updating the linkedlist
5.EXIT
enter your choice3
displaying the data elements
1
20
*************MENU******************
1.add data to the linked list 
2.delete data
3.display the data
4.updating the linkedlist
5.EXIT
enter your choice5
imvizag@administrator-ThinkCentre-M82:~/datastructures$ */

