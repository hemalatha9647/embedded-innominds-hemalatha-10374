/*******************************************************************************************
  NAME			CONTACT NO.		EMAIL-ID			EMPLOYEE ID
--------------------------------------------------------------------------------------------
  HEMALATHA BUSAM	8333097869		hemalatha.busam@gmail.com	10374

This program is for the bit manipulation where the user can set,clear and compliment a particular
bit on the provided integer data type and can see the binary version of the data before and after
 bit manipulation 
***********************************************************************************************
*/

#include<iostream>
#include<stdlib.h>

using namespace std;
//creating a method 
class BitManipulation {
	public:
		//declaring the  variable
		int variable;
		//parametrised constructor
		BitManipulation(int variable)
		{
			this->variable = variable;	
		}
		//method to display the bits in a variable
		void displayVariable()
		{	
			int i;
			for(i=31;i>=0;i--)
				cout<<" "<<((variable>>i)&1);		
			cout<<endl;	
		}
		//method to set paticular bit in a number
		void setBit(int bitpos)
		{	cout<<"binary representation  of number before manipulation"<<endl;
			displayVariable();
			variable = variable|(1<<bitpos);
			cout<<"binary representation  of number after manipulation"<<endl;
			displayVariable();
		}
		//method to clear a particular bit in a number
		void clearBit(int bitpos)
		{
			cout<<"binary representation  of number before manipulation"<<endl;
			displayVariable();
			variable = variable&(~(1<<bitpos));
			cout<<"binary representation  of number after manipulation"<<endl;
			displayVariable();
		}
		//method to complement a partivcular bit in a number
		void complementBit(int bitpos)
		{	

			cout<<"binary representation  of number before manipulation"<<endl;
			displayVariable();
			variable = variable^(1<<bitpos);
			cout<<"binary representation  of number after manipulation"<<endl;
			displayVariable();
		}
};//end of BitManipulation class
int main()
{	
	//decalaring the variables
	int variable;
	char choice;
	int bitposition;
	//reading the variable
	cout<<"enter the integer variable"<<endl;
	cin>>variable;
	//reading the bitposition to be manipulated
	//	cout<<"enter the bit position to manipulate"<<endl;
	//	cin>>bitposition;
	BitManipulation obj(variable);//creating an object
	while(1)
	{
		cout<<endl<<"*****MENU*******"<<endl;
		cout<<"1.Set a bit"<<endl<<"2.Clear a bit"<<endl<<"3.Complement a bit"<<endl<<"4.Exit"<<endl;
		cout<<"enter your choice";
		cin>>choice;
		switch(choice)
		{
			case '1':cout<<"enter the bit position to manipulate"<<endl;
				 cin>>bitposition;
				 obj.setBit(bitposition);
				 break;
			case '2':cout<<"enter the bit position to manipulate"<<endl;
				 cin>>bitposition;
				 obj.clearBit(bitposition);
				 break;
			case '3': cout<<"enter the bit position to manipulate"<<endl;
				  cin>>bitposition;
				  obj.complementBit(bitposition);
				  break;
			case '4':exit(0);
			default:cout<<"INVALID CHOICE";
		}
	}
}
/******************************output of the program*****************
enter the integer variable
10

*****MENU*******
1.Set a bit
2.Clear a bit
3.Complement a bit
4.Exit
enter your choice1
enter the bit position to manipulate
3
binary representation  of number before manipulation
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1 0
binary representation  of number after manipulation
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1 0

*****MENU*******
1.Set a bit
2.Clear a bit
3.Complement a bit
4.Exit
enter your choice1
enter the bit position to manipulate
5
binary representation  of number before manipulation
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1 0
binary representation  of number after manipulation
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1 0 1 0

*****MENU*******
1.Set a bit
2.Clear a bit
3.Complement a bit
4.Exit
enter your choice2
enter the bit position to manipulate
5
binary representation  of number before manipulation
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1 0 1 0
binary representation  of number after manipulation
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1 0

*****MENU*******
1.Set a bit
2.Clear a bit
3.Complement a bit
4.Exit
enter your choice3
enter the bit position to manipulate
3
binary representation  of number before manipulation
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1 0
binary representation  of number after manipulation
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0

*****MENU*******
1.Set a bit
2.Clear a bit
3.Complement a bit
4.Exit
enter your choice4
*/
