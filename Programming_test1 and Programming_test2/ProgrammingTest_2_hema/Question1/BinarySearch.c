/******************************************************************************************************************************
 This program is a implementation of searching data taken from the user using binary search method where we divide the 
 sorted array into two half's if the entered data is less then the mid value. serach in the left half by recursively dividing 
 the half in again two half's this continues till only one element left in the array. If data is not found '-1' is returned. 
 ******************************************************************************************************************************/

#include<stdio.h>

//function to search the data in the array using binary search
int binarySearch(int* arr,int left,int right,int data){
	int mid;
	//logic for binary search
	if(right>=left){
		mid = left+(right-left)/2;
			if(arr[mid]==data)
				return mid;
			else if(arr[mid]>data)
				return binarySearch(arr,left,mid-1,data);
			else
				return binarySearch(arr,mid+1,right,data);
	}
	return -1;
}

//driver program to run the above method
int main(){
	//declaring the variables
	int data,result;
	//declaring and initilazing the array
	int arr[]={3,5,7,9,10,14,16,35,67,89,90};
	int size = sizeof(arr)/sizeof(arr[0]);
	//reading the to data from the user
	printf("Enter the data\n");
	scanf("%d",&data);
	//calling the binarySearch function
	result = binarySearch(arr,0,size-1,data);
	if(result == -1){//condition to check whether data  exist or not
		printf("Data does not exist\n");
	}else{
		printf("%d is at position %d\n",data,result+1);
	}
}
