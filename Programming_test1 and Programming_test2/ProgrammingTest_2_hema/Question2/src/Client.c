/***********************************************************************************************************************************
 This program is implementation of client file for the chat application where client continuosly read data from server using threads.
 Process terminates if server or client sends "bye".
 ***********************************************************************************************************************************/

#include<headers.h>
int i,n;
char buf[1024];
int sockfd;
//function to print the error message
void error(char *msg){
	perror(msg);
	exit(0);
}

//function to writre data to server
void* writeToServer(void*args){
        while(1){
                memset(buf,0,1024);
                fgets(buf,1024,stdin);
                n=write(sockfd,buf,strlen(buf));
                if(n<0){
                        error("error in wroting\n");
                }
                i=strncmp("bye",buf,3);
                if(i==0){
                        exit(0);
                }
        }
        return NULL;
}

//function to read data from server
void* readFromServer(void*args){
        while(1){
                memset(buf,0,sizeof(buf));
                n=read(sockfd,buf,1024);
                if(n<0){
                        error("error in reading\n");
                }
		printf("%s",buf);
                i=strncmp("bye",buf,3);
                if(i==0)
                        exit(0);
        }
        return NULL;
}
//driver program to run above methods
int main(int argc,char **argv){
	//declaring the variables
	struct sockaddr_in serv_addr;
	struct hostent *server;
	pthread_t threadid1,threadid2;
	//condition to check sufficient arguments provided or not
	if(argc<3){
		error("insufficient arguments\n");
	}
	//socket creation
	sockfd = socket(AF_INET,SOCK_STREAM,0);
	if(sockfd<0){
		error("error in socket creation");
	}
	memset((char*)&serv_addr,0,sizeof(serv_addr));
	server = gethostbyname(argv[1]);
	if(server == NULL)
		error("error in getting host name\n");
	//initializing the sockaddr_in structure varibles  
	strncpy((char*)server->h_addr,(char*)&serv_addr.sin_addr.s_addr,server->h_length);
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(atoi(argv[2]));
	//connecting to server
	if(connect(sockfd,(struct sockaddr*)&serv_addr,sizeof(serv_addr))<0){
		error("error in connecting\n");
	}
	//creating threads
	pthread_create(&threadid1,NULL,writeToServer,NULL);
        pthread_create(&threadid2,NULL,readFromServer,NULL);
        pthread_join(threadid1,NULL);
        pthread_join(threadid2,NULL);
	close(sockfd);
	return 0;
}
