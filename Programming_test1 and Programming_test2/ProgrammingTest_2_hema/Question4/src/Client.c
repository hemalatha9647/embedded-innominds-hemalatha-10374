/******************************************************************************************************************************************
 This program is a implementation of client server program for downloading a file from the server where client asks for the file it exists 
 then client displays it on console. This is client file.
******************************************************************************************************************************************/ 

#include<headers.h>
int i,n;
char buf[1024];
int sockfd;
//function to print the error message
void error(char *msg){
	perror(msg);
	exit(0);
}

//driver program to run above methods
int main(int argc,char **argv){
	//declaring the variables
	struct sockaddr_in serv_addr;
	struct hostent *server;
	//condition to check sufficient arguments provided or not
	if(argc<3){
		error("insufficient arguments\n");
	}
	//socket creation
	sockfd = socket(AF_INET,SOCK_STREAM,0);
	if(sockfd<0){
		error("error in socket creation");
	}
	memset((char*)&serv_addr,0,sizeof(serv_addr));
	server = gethostbyname(argv[1]);
	if(server == NULL)
		error("error in getting host name\n");
	//initializing the sockaddr_in structure varibles  
	strncpy((char*)server->h_addr,(char*)&serv_addr.sin_addr.s_addr,server->h_length);
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(atoi(argv[2]));
	//connecting to server
	if(connect(sockfd,(struct sockaddr*)&serv_addr,sizeof(serv_addr))<0){
		error("error in connecting\n");
	}
	memset(buf,0,sizeof(buf));
	fgets(buf,1024,stdin);
	write(sockfd,buf,strlen(buf));
	memset(buf,0,sizeof(buf));
	read(sockfd,buf,1024);
	puts(buf);
	close(sockfd);
	return 0;
}
