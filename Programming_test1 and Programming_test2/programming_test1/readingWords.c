/*****************************************************************************************************************
 This program is implementation of reading the words from the source file and writing then to destination file.
 *****************************************************************************************************************/

#include<stdio.h>
#include<stdlib.h>
int main(){
	//declaring the variables
	FILE *fp,*fp1;
	char buf[10],ch;
	int count=0;
	//opning the source file in read mode
	fp = fopen("giventext.txt","r");
	if(fp == NULL){
		printf("datafile not found");
		exit(0);
	}
	//logic for counting the numbers of words
	while((ch=fgetc(fp))){
		if(ch==32||ch=='\n'){
			count++;
		}
		if(ch==EOF)
			break;
	}
	//opening the destination file in append mode
	fp1 = fopen("newtext.txt","a");
	rewind(fp);
	//logic for writing the data into destination file
	for(int i=0;i<count;i++){
		fscanf(fp,"%s",buf);
		fprintf(fp1,"%s\n",buf);
                printf("%s",buf);

        }
	//closing the file descripters
	fclose(fp);
	fclose(fp1);
	return 0;
}
