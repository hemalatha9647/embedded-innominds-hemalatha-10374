#include<headers.h>
void BinaryEquivalent(int);
//function to set the 8th bit
void setbit(int data){
	data = data|(1<<8);
	printf("data %d",data);
	BinaryEquivalent(data);
}

//function to count number of bits set
void countNumOfSetBits(int data){
	int i,count=0;
	for(i=31;i>=0;i--){
		if((data>>i)&1)
			count++;
	}
	printf("number of bits set = %d",count);
}

//function to find the binary equivalent of a number
void BinaryEquivalent(int data){
	int i;
	for(i=31;i>=0;i--){
		printf("%d",(data>>i)&1);
	}
}

//two's complement data
void complement(int data){
	data = ~data;
	data = data+1;
	BinaryEquivalent(data);
} 
