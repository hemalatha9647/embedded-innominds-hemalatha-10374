/***********************************************************************************************************************************
 This program is implementation of setting 8th bit, counting, binary equivalent and complementing the bit
 *********************************************************************************************************************/
#include<headers.h>
int main(){
	char choice;
	int data;
	while(1){
		printf("\n****MENU****\n");
		printf("1.set 8th bit\n2.counting no of set bits\n3.printing binary equivalent\n4.exit\n5.complement\n");
		printf("Enter your choice\n");
		__fpurge(stdin);
		scanf("%c",&choice);
		switch(choice){
			case '1':printf("Enter the data\n");
				 scanf("%d",&data);
				 setbit(data);
				 break;
			case '2':printf("Enter the data\n");
                                 scanf("%d",&data);
				 countNumOfSetBits(data);
				 break;
			case '3':printf("Enter the data\n");
                                 scanf("%d",&data);
				 BinaryEquivalent(data);
				 break;
			case '5':printf("Enter the data\n");
                                 scanf("%d",&data);
                                 complement(data);
                                 break;
			case '4':exit(0);
			default:printf("invalid input\n");

		}
	}
	return 0;
}

