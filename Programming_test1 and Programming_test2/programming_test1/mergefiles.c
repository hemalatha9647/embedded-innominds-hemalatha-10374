/********************************************************************************************************************************
 This program is implementation reading the alternatives lines from the two source files  by opening them in read mode and writing
 them to source file by opening the file in append mode.
 ********************************************************************************************************************************/
#include<stdio.h>
#include<string.h>
int main(){
	//decalring the file pointers
	FILE *fp,*fp1,*fp2;
	//decalring the variables
	char buf[100],buf1[100];
	//opening the source files in read mode
	fp = fopen("a.txt","r");
	fp1 = fopen("b.txt","r");
	//opening the resulant file in append mode
	fp2 = fopen("c.txt","a");
	//logic for reading the alternative lines
	/*while(fgets(buf1,100,fp1)){
 		memset(buf,0,100);
		fgets(buf,100,fp);
		fputs(buf,fp2);
		fputs(buf1,fp2);
	}*/
	
	//closing th file descriptors
	fclose(fp);
	fclose(fp1);
	fclose(fp2);
	return 0;
}
