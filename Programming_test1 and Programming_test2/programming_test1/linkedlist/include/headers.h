#include<stdio.h>
#include<stdlib.h>
//declaring the structure
struct student{
	//declaring the structure variables
	int rollnum;
	char name[50];
	struct student *next;//self-referential structure
};
//function declarations
void insertDataAtTheBeginning(struct student**);
void displayingTheData(struct student *);
