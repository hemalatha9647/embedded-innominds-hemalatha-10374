#include<headers.h>
//function to add data at the beginning
void insertDataAtTheBeginning(struct student **ptr){
	//declaring the variable of type structure student
	struct student *newnode = (struct student*)malloc(sizeof(struct student));
	printf("Enter the student roll number\n");
	scanf("%d",&newnode->rollnum);
	printf("Enter the name of the student\n");
	scanf("%s",newnode->name);
	newnode->next = *ptr;
	*ptr = newnode; 
}

//function for displaying the student details
void displayingTheData(struct student *ptr){
	printf("\n*****STUDENT DETAILS*****\n");
	printf("ROLL NUMBER      STUDENT NAME\n");
	while(ptr){
		printf("    %d               %s\n",ptr->rollnum,ptr->name);
		ptr = ptr->next;
	}
}
