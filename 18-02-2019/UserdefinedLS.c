/**********************************************************************************************************************
 NAME                    EMPLOYEE:ID                              EMAILID                                 PHONE NUMBER
 ----------------------------------------------------------------------------------------------------------------------
 BUSAM HEMALATHA          10374                             hemalatha.busam@gmail.com                    8333097869
 ---------------------------------------------------------------------------------------------------------------------
 This program is the implementation of userdefined ls command
 *********************************************************************************************************************/
#include<stdio.h>
#include<dirent.h>
#include<stdlib.h>
#include<string.h>
int main()
{
	DIR *dp;
	struct dirent *ptr;
	char (*p)[30]={0},temp[30];
	int i,count=0;
	dp=opendir("./");
	while(ptr=readdir(dp))
	{
		if(ptr->d_name[0]!='.')
		{
			p=realloc(p,(count+1)*30);
			strcpy(temp,ptr->d_name);
			strcpy(p[count++],temp);
		}
	}
	printf("\n");
	for(i=0;i<count;i++)
		printf("%s\t",p[i]);

	printf("\n");

	return 0;
}

/******************************************output of the above program****************************************************************
 

MaxSumInArray.c	SingleLinkedList	RemoveOccuranceinString.c	a.out	InfiniteLoopinList.c	FloatValueReverse.c	path.c	FindMergeinList.c	UserDefinedLScommand.c	bit	SuminArray.c	12-02-19(k)	UserdefinedStringFunctions.c	CircularQueue	convolution.cStringReverse.c	RootsOfQudratic.c	12-02-19(v)	Palindrome.c	TrailingZeroinFact.c	Calculator	StringConversion.c	MatrixRotation.c	implementwordcount	12-02-19(h)	MatrixMultiplication.c	mergefile	SingleLinkedList.c	fileconentreverse	
***************************************************************************************************************************************/
