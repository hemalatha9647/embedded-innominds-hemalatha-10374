/***************************************************************************************************************
NAME                      EMAILID                           PHONE NUMBER                   EMPID
---------------------------------------------------------------------------------------------------------------
BUSAM HEMA LATHA        hemalatha.busam@gmail.com       8333097869                10374
-----------------------------------------------------------------------------------------------------------------
This program is to write content to the console ,reading data from the console and writing data in to another file
*****************************************************************************************************************/

#include<stdio.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<string.h>

int main() {

	int fp1,size;
	char buff[100];
	fp1=open("sample.txt",O_WRONLY | O_CREAT);
	if (fp1 == -1)

		printf("Error in opening the file  \n"); 
	else 
			printf("input file opened in read mode\n");
	printf("write the conent that should be written into file\n");
	printf("..............press (CNTRL+c) to exit the loop .......\n");
	while(1) {
	size=read(0,buff,sizeof(buff));
	//writing from input file to output file
	write(fp1,buff,size);
	}
	printf("writing to output file using system call successfull\n");
	close(fp1);
	return 0;

}

/********************OUTPUT OF THE ABOVE PROGRAM****************************************
 input file opened in read mode
write the conent that should be written into file
..............press (CNTRL+c) to exit the loop .......
hello
iam fine
^C

*****************************************************************************************/

