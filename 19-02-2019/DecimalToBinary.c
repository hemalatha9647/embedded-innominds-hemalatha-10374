/**********************************************************************************************************
NAME                      EMAILID                            PHONE NUMBER                  EMPID
------------------------------------------------------------------------------------------------------------
BUSAM HEMA LATHA       hemalatha.busam@gmail.com     	      8333097869                  10374
------------------------------------------------------------------------------------------------------------
This program is to convert decimal number to equivalent binary number  using recursion
************************************************************************************************************/
#include<stdio.h>
#include<stdlib.h>
//it converts given decimal number in  binary number and returns the value  
int decimalToBinary(int num) {
	if(num==0) 
		return 0;
	else 
		return (num%2+10 * (decimalToBinary(num/2)));

}

int main() {
	int num;
	printf("enter the number\n");
	scanf("%d",&num);
	int a=decimalToBinary(num);
	printf("the binary  number is %d  for the given decimal number  %d\n",a,num);
	return 0;
}
/*************************************OUTPUT OF THE ABOVE PROGRAM**************************************
 enter the number
9
the binary  number is 1001  for the given decimal number  9
********************************************************************************************************/
