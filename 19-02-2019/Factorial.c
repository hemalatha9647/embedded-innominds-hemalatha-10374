/*********************************************************************************************************
NAME                      EMAILID                            PHONE NUMBER                  EMPID
------------------------------------------------------------------------------------------------------------
BUSAM HEMA LATHA        hemalatha.busam@gmail.com                 8333097869                   10374
------------------------------------------------------------------------------------------------------------
This program is to find factorial of a given number using recursion
************************************************************************************************************/
#include<stdio.h>
//this function returns the factorial of given number
 int fact(int num) {

	 if(num==0 || num==1) 
		 return 1;
	 else
		return (num*fact(num-1));
 }
int main() {
	int num;
	printf("enter the number to find the factorial :\n");
	scanf("%d",&num);
	while(num<0) {
		printf("factorial cannot be negative......re enter \n");
		scanf("%d",&num);
	}
	int a=fact(num);
	printf("the factorial for the given number %d is: %d\n",num,a);
	return 0;
}
/******************************************OUTPUT OF THE ABOVE PROGRAM*********************************************
enter the number to find the factorial :
8
the factorial for the given number 8 is: 40320
********************************************************************************************************************/
