/***************************************************************************************************************
NAME                      EMAILID                          PHONE NUMBER                   EMPID
------------------------------------------------------------------------------------------------------------
BUSAM HEMA LATHA       hemalatha.busam@gmail.com     	    8333097869                    10374
---------------------------------------------------------------------------------------------------------------
This program is to find an data in the given array using binary search function with using recursion concept.
****************************************************************************************************************/

#include<stdio.h>
#include<stdlib.h>

//it search the data in the given array using binary search
int binarysearch(int low,int high,int *array,int data) {

	int mid;

	if(low>high)
		return -1;
	mid=(low+high)/2;

	if(array[mid]==data)
		return mid;

	else if(array[mid]>data) {
		high = mid-1;
		binarysearch(low,high,array,data);
	}
	else {
		low=mid+1;
		binarysearch(low,high,array,data);
	}

}
//sorting the  array elements in ascending order using bubble sort
int * sorting(int *array,int size) {

	int i,j,temp;

	for(i=0;i<size;i++) {
		for(j=0;j<size-1;j++) {
			if(array[j] > array[j+1]) {
				temp = array[j];
				array[j]=array[j+1];
				array[j+1]=temp;
			}
		}
	
	}

	return array;
}
void displayArray(int array[],int size){
	int i;
	for(i=0;i<size;i++) {
		printf("%d\t",array[i]);
	}
	printf("\n");
}
int main() {
	int array[100];
	int size,i,data,index;
	printf("enter the size of array\n");
	scanf("%d",&size);
	while(size<=0) {
		printf("size cannot be negative.....re enter\n");
		scanf("%d",&size);
	}
	printf("enter the elements into array \n");
	for(i=0;i<size;i++) {
		scanf("%d",&array[i]);
	}
	printf("elements in array before sorting are:");
	displayArray(array,size);
	sorting(array,size);
	printf("elements in array after sorting are:");
	displayArray(array,size);
	printf("enter the data to be searched\n");
	scanf("%d",&data);

	index=binarysearch(0,size-1,array,data);

	if(index>=0)
		printf("the data  %d is found at index  %d  in the given array \n",data,index);
	else
		printf("the data %d  is not found in the given array \n",data);
	return 0;

}
/*********************************OUTPUT OF THE ABOVE PROGRAM*************************************
enter the size of array
6
enter the elements into array 
8
9
1
2
4
7
elements in array before sorting are:8	9	1	2	4	7	
elements in array after sorting are:1	2	4	7	8	9	
enter the data to be searched
4
the data  4 is found at index  2  in the given array  
***************************************************************************************************/
