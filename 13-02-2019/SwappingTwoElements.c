/*************************************************************************************************************************
 NAME                           EMAIL-ID                        PHONE NUMBER                    EMPLOYEE-ID
 -------------------------------------------------------------------------------------------------------------------------
 BUSAM HEMA LATHA           hemalatha.busam@gmail.com         8333097869                            10374
 -------------------------------------------------------------------------------------------------------------------------
 This program is the implementation of swapping the two elememts using X-OR operation.
**************************************************************************************************************************/

#include<stdio.h>
int main(){
	//declaring the variables
	int data1,data2;
	printf("Enter first element\n");
	scanf("%d",&data1);
	printf("Enter second element\n");
	scanf("%d",&data2);
	printf("***Before swapping***\nfirst element = %d\nsecond element =  %d\n",data1,data2);
	//logic for swapping the elements
	data1^=data2^=data1^=data2;
	printf("\n***After swapping***\nfirst element = %d\nsecond element = %d\n",data1,data2);
}
/*************************************OUTPUT FOR THE ABOVE PROGRAM************************************************************
 Enter first element
5
Enter second element
6
***Before swapping***
first element = 5
second element =  6

***After swapping***
first element = 6
second element = 5
*********************************************************************************************************************************/
