/*************************************************************************************************************************
 NAME                           EMAIL-ID                        PHONE NUMBER                    EMPLOYEE-ID
 -------------------------------------------------------------------------------------------------------------------------
 BUSAM HEMA LATHA           hemalatha.busam@gmail.com         8333097869                            10374
 -------------------------------------------------------------------------------------------------------------------------
 This program is the implementation checking given number is even or odd.
**************************************************************************************************************************/

#include<stdio.h>

int main(){
	//declaring the variables
	int data;
	printf("Enter the data\n");
	scanf("%d",&data);
	//condition for even or odd
	if(data&1){
		printf("odd number\n");
	}else{
		printf("Even number\n");
	}
	return 0;
}
/***************************OUTPUT FOR THE ABOVE PROGRAM************************************
 Enter the data
5
odd number
busam@busam-Lenovo-G505:~/13-2-2019$ ./a.out
Enter the data
4
Even number
busam@busam-Lenovo-G505:~/13-2-2019$ ./a.out
Enter the data
11
odd number
busam@busam-Lenovo-G505:~/13-2-2019$ 
******************************************************************************************/
