/*************************************************************************************************************************
 NAME                           EMAIL-ID                        PHONE NUMBER                    EMPLOYEE-ID
 -------------------------------------------------------------------------------------------------------------------------
 BUSAM HEMA LATHA           hemalatha.busam@gmail.com         8333097869                            10374
 -------------------------------------------------------------------------------------------------------------------------
 This program is the implementation of finding the missing element in the array.
**************************************************************************************************************************/

#include<stdio.h>

int main(){
	int size;
	printf("Enter the array size\n");
	scanf("%d",&size);
	//declaring the variables
	int arr[size-1],total=0,total1=0;
	//logic for adding the total number array elements
	for(int i=1;i<=size;i++){
		total1 = total1+i;
	}
	//logic for adding the array elements
	for(int i=0;i<size-1;i++){
		printf("Enter the data\n");
		scanf("%d",&arr[i]);
		total = total+arr[i];
	}
	printf("Missing data is %d\n",(total1-total));
	return 0;
}

/****************************************OUTPUT FOR THE ABOVE PROGRAM*******************************************************
 Enter the array size
5
Enter the data
4
Enter the data
3
Enter the data
2
Enter the data
1
Missing data is 5
busam@busam-Lenovo-G505:~/13-2-2019$ 
****************************************************************************************************************************/
