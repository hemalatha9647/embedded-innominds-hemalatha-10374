/************************************************************************************************************************************
 	NAME                               EMAIL ID                         MOBILE NUMBER              EMPLOYEE ID
 ------------------------------------------------------------------------------------------------------------------------------------
	BUSAM HEMA LATHA		hemalatha.busam@gmail.com	      8333097869		   10374
-------------------------------------------------------------------------------------------------------------------------------------
This program counts the number of possible paths for a given number of rows and columns
************************************************************************************************************************************/

#include<stdio.h>
#include<stdlib.h>
int main(){
	//decalaring the variables
	int rows,columns,i,j;
	//reading number of rows and column values
	printf("enter number of rows and columns\n");
	scanf("%d %d",&rows,&columns);
	int arr[rows][columns];//2D-array declaration
	//logic for finding the number of possible for a given number of rows and columns
	for(i=0;i<rows;i++)
	{
		for(j=0;j<columns;j++){
				if((i==0)||(j==0)){
					arr[i][j]=1;
				}else{
					arr[i][j]=arr[i-1][j]+arr[i][j-1];
					}
				if((i==rows-1)&&(j==columns-1)){
					printf("Number of possible paths for %d rows and %d columns : %d\n",rows,columns,arr[i][j]);
		}
	}
	}
}

/***************************************************OUTPUT FOR THE ABOVE PROGRAM***************************************************
enter number of rows and columns
5
5
Number of possible paths for 5 rows and 5 columns : 70
busam@busam-Lenovo-G505:~/assignments$ ./a.out
enter number of rows and columns
7
5
Number of possible paths for 7 rows and 5 columns : 210
*/
