/**************************************************************************************************
NAME                      EMAILID                            PHONE                   EMPID
------------------------------------------------------------------------------------------------------------
HEMALATHA BUSAM       8333097869              hemalatha.busam@gmail.com       10374

PURPOSE:
----------------
This program gives the output along with content present in source code. 
***********************************************************************************************/
#include <stdio.h>
#include <stdlib.h>

int main(void) {

	FILE *fp = NULL;
	int ch;
	fp = fopen(__FILE__,"r");	
	//This give the file name which is we were given.
	//printf("%s\n",__FILE__);
	if(fp == NULL) {
		perror("fp");
		return -1;
	}
	printf("FILE OPENED SUCCESSFULLY\n");
	printf("MY NAME IS HEMALATHA\n");
	printf("------------------------------------------------\n");
	while((ch=fgetc(fp)) != -1) {
		printf("%c",ch);
	}
	fclose(fp);
	return 0;
}
/***************************OUTPUT******************************
#include <stdio.h>
#include <stdlib.h>

int main(void) {

	FILE *fp = NULL;
	int ch;
	fp = fopen(__FILE__,"r");	
	//This give the file name which is we were given.
	//printf("%s\n",__FILE__);
	if(fp == NULL) {
		perror("fp");
		return -1;
	}
	printf("FILE OPENED SUCCESSFULLY\n");
	printf("MY NAME IS HEMALATHA\n");
	printf("------------------------------------------------\n");
	while((ch=fgetc(fp)) != -1) {
		printf("%c",ch);
	}
	fclose(fp);
	return 0;
}

 ****************************************************************/
