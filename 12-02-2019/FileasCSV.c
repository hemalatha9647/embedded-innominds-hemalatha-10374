/***************************************************************************************

NAME                 PHONENUM                  EMAILID                           EMPID
-------------------------------------------------------------------------------------------
HEMALATHA BUSAM       8333097869              hemalatha.busam@gmail.com       10374

PURPOSE:
-----------------
In This program we are using CSV file to store employee details, writing employee details 
in to the file, the read from the file and printing the details of employee on the screen  

*******************************************************************************************/
#include <stdio.h>
#include <stdlib.h>

typedef struct employee {
	int id;
	char name[20];
}emp;

int main(void) {

	FILE *fp=NULL;
	int ch;
	int cnt=3;
	emp *temp=(emp*)malloc(sizeof(emp)*cnt);
	if(temp == NULL) {
		printf("Failed to memory allocation \n");
		exit(0);
	}
	//opening the file to write
	if((fp = fopen("csv.csv","a+")) == NULL) {
		printf("Failed to open the file\n");
		exit(0);
	}
	else
		printf("FILE OPENED SUCCESSFULLY\n");
	while(cnt!=0) {
		printf("Enter  student id :\n");
		scanf("%d",&temp->id);
		getchar();
		fprintf(fp,"%d",temp->id);
		printf("Enter student  name :\n");
		fgets(temp->name,20,stdin);
		fputs(",",fp);
		fputs(temp->name,fp);
		fputs("\n",fp);

		
		cnt--;
	}
	free(temp);
	//closing the file
	fclose(fp);


	//opening the file to read the contents
	if((fp = fopen("csv.csv","r")) == NULL) {
		printf("Failed to open file\n");
		exit(0);
	}
	else
		printf("OPENED SUCCESSFULLY\n");
	printf("printing the conents of csv file\n");
	printf("--------------------------------------\n");
	while((ch=fgetc(fp))!=EOF){
		printf("%c",ch);
	}

	printf("------------------------------------------\n");
	//closing the csv file
	fclose(fp);
	return 0;
}
/*******************OUTPUT***********************************
FILE OPENED SUCCESSFULLY
Enter  student id :
1
Enter student  name :
hema
Enter  student id :
2
Enter student  name :
sai
Enter  student id :
3
Enter student  name :
vidya
OPENED SUCCESSFULLY
printing the conents of csv file
--------------------------------------
1,hema

2,sai

3,vidya
------------------------------------------
**************************************************************/
