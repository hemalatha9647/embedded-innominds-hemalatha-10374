/***********************************************************************************************

  NAME                 PHONENUM                  EMAILID                           EMPID
HEMALATHA BUSAM       8333097869              hemalatha.busam@gmail.com       10374

PURPOSE :
-----------------------------------------------------------------------------------------------
	In This program we are already placing the data into the file,reading the elements 
from csv file, storing the data in 2D matrix,and printing the data in the matrix
************************************************************************************************/
#include<stdio.h>
int main() {
	FILE *fp1=NULL;
	int ch,count=0;
	if((fp1=fopen("matrix.csv","r")) == NULL) {
		printf("Failed to open the file.........\n");
		return -1;
	}
	else
		printf("File1 has  Opened succesfully\n");

	while((ch = fgetc(fp1)) != -1 ) {
		if(ch==',' || ch==10){
		}else {
			count+=1;
		}
	}
	printf("the elements in the CSV file are : %d\n",count);
	rewind(fp1);
	int rows,columns;
	int i=0,j=0;
	printf("enter the rows of a matrix\n");
	scanf("%d",&rows);
	printf("enter the columns of a matrix\n");
	scanf("%d",&columns);
	//if product of rows and columns are less than the count then asking the user to enter the rows and columns values such that product i		s not less than count
	while(rows*columns<count) {
		printf("the entered rows and columns product %d is less than total numners %d \n",rows*columns,count);
		printf("enter the rows of a matrix\n");
		scanf("%d",&rows);
		printf("enter the columns of a matrix\n");
		scanf("%d",&columns);
	}
	int matrix[rows][columns];
	//initialing the elements 
	while((ch = fgetc(fp1)) != -1) {
		if(ch==',' || ch==10){
			j+=1;
			if(j>=columns) {
				j=0;
				i+=1;
			}
		}else {
			matrix[i][j]=ch-48;
		}
	}
	//making remaining elements in the matrix 0,if rows*columns>count
	for(i;i<=rows;i++) {
		for(j;j<=columns;j++) {
			matrix[i][j]=0;
		}
		j=0;
	}
	//printing matrix elements
	printf(" the elemenets in the matrix wriiten from CSV file are :\n");
	for(i=0;i<rows;i++) {
		for(j=0;j<columns;j++) {
			printf("%d ",matrix[i][j]);
		}
		printf("\n");
	}
	return 0;
}
/*******************OUTPUT********************************************
File1 has  Opened succesfully
the elements in the CSV file are : 25
enter the rows of a matrix
6
enter the columns of a matrix
2
the entered rows and columns product 12 is less than total numners 25 
enter the rows of a matrix
5
enter the columns of a matrix
5
 the elemenets in the matrix wriiten from CSV file are :
1 2 3 4 5 
6 7 8 9 2 
1 2 3 4 5 
2 3 4 5 6 
4 5 7 6 8 
************************************************************************/

