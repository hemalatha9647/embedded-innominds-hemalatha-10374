/************************************************************************************************************************
NAME                         PHONE NUMBER                      EMPLOYEE ID                            EMAIL ID                            
BUSAM HEMA LATHA                8333097869                       10374                                  hemalatha.busam@gmail.com

This program is implementation of queue using Linked List where user can add data from the front
and pop the data from rear end.
*************************************************************************************************************************/


#include<iostream>
#include<stdlib.h>

using namespace std;
//creating a structure of type node
struct node{
	//declaring the variables
	int data;
	struct node *next;
};
int length=0;
//creating a class of Structure type
class Queue{
	struct node *front;
	struct node *rare;
	public:
	//default constructor
	Queue(){
		front = NULL;
		rare = NULL;
	}
	//member functions
	void enque(int);
	int deque();
	void displayingData();
};

//method to add the data into the queue
void Queue ::enque(int data){
	node *newnode = new node;
	newnode->data = data;
	newnode->next = NULL;
	length++;
	if(front == NULL)
	{
		front = newnode;
		rare = newnode;
		newnode->next = NULL;
		return;
	}
	rare->next = newnode;
	rare = newnode;
	return;
}

//method to display the data from the queue
void Queue :: displayingData()
{
	if(front==NULL)
	{
		cout<<"Queue is  underflow"<<endl;
	}else{
		 node *temp;
		for(temp = front;temp!=NULL;temp=temp->next){
			cout<<temp->data<<endl;	
			}
	}
	

}

//method to  delete data from the queue
int Queue :: deque()
{
	int data;
	if(front==NULL)	
	{
		cout<<"Queue is empty"<<endl;
		return 0;
	}
	node  *temp = front;
		front = front->next;
		delete temp;
		temp = NULL;
		return data;
}

 main()
{
	//declaring the variables
	int choice;
	int data;
	Queue queue;
	while(1){
		cout<<"************MENU**************"<<endl;
		cout<<"1.add data to queue "<<endl<<"2.delete data from queue"<<endl<<"3.displaying the data"<<endl<<"4.EXIT"<<endl;
		cin>>choice;
		switch(choice){
			case 1:cout<<"enter the data"<<endl;
				cin>>data;
				queue.enque(data);
				break;
			case 2:data=queue.deque();
				if(data)
					cout<<"deleted data"<<data<<endl;
				break;
			case 3:queue.displayingData();
				break;
			case 4:exit(0);
			default:cout<<"invalid input";
			}
	}
	}
/*****************************************OUTPUT OF THE ABOVE PROGRAM*******************************************************
************MENU**************
1.add data to queue 
2.delete data from queue
3.displaying the data
4.EXIT
1
enter the data
10
************MENU**************
1.add data to queue 
2.delete data from queue
3.displaying the data
4.EXIT
1
enter the data
20
************MENU**************
1.add data to queue 
2.delete data from queue
3.displaying the data
4.EXIT
1
enter the data
30
************MENU**************
1.add data to queue 
2.delete data from queue
3.displaying the data
4.EXIT
3
10
20
30
************MENU**************
1.add data to queue 
2.delete data from queue
3.displaying the data
4.EXIT
2
************MENU**************
1.add data to queue 
2.delete data from queue
3.displaying the data
4.EXIT
3
20
30
************MENU**************
1.add data to queue 
2.delete data from queue
3.displaying the data
4.EXIT
2
************MENU**************
1.add data to queue 
2.delete data from queue
3.displaying the data
4.EXIT
3
30
************MENU**************
1.add data to queue 
2.delete data from queue
3.displaying the data
4.EXIT
4
imvizag@administrator-ThinkCentre-M82:~/datastructures$ */


