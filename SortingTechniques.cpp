/***********************************************************************************************************************************
    NAME                             MOBILE NUMBER                     EMAIL ID                         EMPLOYEE ID
-----------------------------------------------------------------------------------------------------------------------------------
BUSAM HEMA LATHA                      8333097869                    hemalatha.busam@gmail.com            10374
-----------------------------------------------------------------------------------------------------------------------------------
This is the implementation of bubble sort and insertion sort


**********************************************************************************************************************************/


#include<iostream>
#include<stdlib.h>

using namespace std;

//creating a Sorting Techniques class
class SortingTechniques{
	//declaring the variables
	int *array;
	int size;
	int i,j;
	public:
	//parameterised constructor
	SortingTechniques(int size){
		this->size=size;
		array = new int[size];
		cout<<"enter the array elements";
		for(i=0;i<size;i++){
			cin>>array[i];
		}
	}
	//member functions
	void bubbleSort();
	void insertionSort();
	void swapTheElements(int&,int&);
	void printArray();
	void myMenu();

};

//method to sort elements using bubble sort
void SortingTechniques :: bubbleSort(){
	int i,j;
	for(i=0;i<size-1;i++){
		for(j=0;j<size-i-1;j++){
			if(array[j]>array[j+1]){
				swapTheElements(array[j],array[j+1]);		
			}
				
				
		}

	}

}

//void SortingTechniques :: mergePartition()
//method to sort elements using insertion sort
void SortingTechniques :: insertionSort()
{
int key;
	for(i=1;i<size;i++)
	{
		key = array[i];
		j = i-1;
		while(j>=0&&array[j]>key)
		{
			array[j+1]=array[j];
			j = j-1;
		}
		array[j+1] = key;
	}
}

//method to swap elements
void SortingTechniques :: swapTheElements(int &a,int &b )
{
	int temp ;
	temp = a;
	a = b;
	b = temp;	
}

//method to display the elements of an array
void SortingTechniques :: printArray(){
	for(i=0; i< size; i++)
		cout<<array[i]<<" ";
	cout<<endl;
}

//menu to call different methods
void SortingTechniques :: myMenu()
{
	int choice;
        while(1){
                cout<<"*************MENU******************"<<endl;
                cout<<"1.Bubble Sort"<<endl<<"3.Insertion Sort"<<endl<<"4.display the data"<<endl<<"5.EXIT"<<endl;
                cout<<"enter your choice";
                cin>>choice;
                switch(choice){
                        case 1: bubbleSort();
				cout<<"BUBBLE SORT"<<endl<<"elements after sorting"<<endl;
				printArray();
                                break;
                       /* case 2:selectionSort();
				cout<<"SELECTION SORT"<<endl<<"elements after sorting"<<endl;
				printArray();
                              	break;*/
                       	case 3:insertionSort();
				cout<<"INSERTION SORT"<<endl<<"enter the position where data to be deleted";
                                printArray();
                                break;
                        case 4:cout<<"displaying the data elements"<<endl;
                                break;
                        case 5:exit(0);
                        default:cout<<"enter valid input";
                }
}
}

//driver program to test above methods
int main()
{
	int size;
	cout<<"enter the size of an array";
	cin>>size;
	SortingTechniques sort(size);
	sort.myMenu(); 
}

/************************************************OUTPUT OF THE ABOVE PROGRAM************************************
enter the size of an array5
enter the array elements1
8
4
9
5
*************MENU******************
1.Bubble Sort
3.Insertion Sort
4.display the data
5.EXIT
enter your choice1
BUBBLE SORT
elements after sorting
1 4 5 8 9 
*************MENU******************
1.Bubble Sort
3.Insertion Sort
4.display the data
5.EXIT
enter your choice3
INSERTION SORT
enter the position where data to be deleted1 4 5 8 9 
*************MENU******************
1.Bubble Sort
3.Insertion Sort
4.display the data
5.EXIT
enter your choice5
imvizag@administrator-ThinkCentre-M82:~/datastructures$ */

