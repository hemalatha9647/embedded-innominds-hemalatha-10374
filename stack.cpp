/************************************************************************************************************************
  NAME                         PHONE NUMBER                      EMPLOYEE ID                            EMAIL ID                            
BUSAM HEMA LATHA		8333097869                       10374                                  hemalatha.busam@gmail.com

This program is implementation of stack using Linked List where user can add data from end and pop it from the end.
*************************************************************************************************************************/
#include<iostream>
#include<stdlib.h>

using namespace std;
//creating a structure of type node
struct node{
	//declaring the variables
	int data;
	struct node *next;
};
int length=0;
//creating a class of Structure type
class Stack{
	struct node *top;
	public:
	//default constructor
	Stack(){
		top = NULL;
	}
	//member functions
	void push(int);
	int pop();
	void displayingData();
};

//method to add the data into the stack
void Stack :: push(int data){
	node *newnode = new node;
	newnode->data = data;
	newnode->next = NULL;
	length++;
	if(top == NULL)
	{
		top = newnode;
		return;
	}
	node *temp;
	for(temp = top;temp->next!=NULL;temp = temp->next);
	temp->next = newnode;
	return;
}

//method to display the data from the stack
void Stack :: displayingData()
{
	if(top==NULL)
	{
		cout<<"Stack underflow"<<endl;
	}else{
		 node *temp;
		for(temp = top;temp!=NULL;temp=temp->next){
			cout<<temp->data<<endl;
		
			
			}
	}
cout<<"length of the stack is"<<length<<endl;	
}

//method to pop the data from the stack
int Stack :: pop()
{
	node *temp,*prev;
	int data;
	if(top==NULL)	
	{
		cout<<"stack is empty"<<endl;
		return 0;
	}
	if(top->next==NULL){
		data = top->data;
		top = NULL;
		length--;
		return data;
	}
	for(temp=top;temp->next!=NULL;prev = temp, temp = temp->next);
	prev->next = NULL;
	data = temp->data;
	delete temp;
	temp = NULL;
	length--;
	return data;
}

 main()
{
	//declaring the variables
	int choice;
	int data;
	Stack stack;
	while(1){
		cout<<"************MENU**************"<<endl;
		cout<<"1.push data into stack "<<endl<<"2.pop data from the stack"<<endl<<"3.displaying the data"<<endl<<"4.EXIT"<<endl;
		cin>>choice;
		switch(choice){
			case 1:cout<<"enter the data"<<endl;
				cin>>data;
				stack.push(data);
				break;
			case 2:data=stack.pop();
				if(data)
					cout<<"poped data"<<data<<endl;
				break;
			case 3:stack.displayingData();
				break;
			case 4:exit(0);
			default:cout<<"invalid input";
			}
}
	}

/*******************************************OUTPUT OF THE ABOVE PROGRAM*******************************************
************MENU**************
1.push data into stack 
2.pop data from the stack
3.displaying the data
4.EXIT
1
enter the data
10
************MENU**************
1.push data into stack 
2.pop data from the stack
3.displaying the data
4.EXIT
1
enter the data
20
************MENU**************
1.push data into stack 
2.pop data from the stack
3.displaying the data
4.EXIT
1
enter the data
30
************MENU**************
1.push data into stack 
2.pop data from the stack
3.displaying the data
4.EXIT
3
10
20
30
length of the stack is3
************MENU**************
1.push data into stack 
2.pop data from the stack
3.displaying the data
4.EXIT
2
poped data30
************MENU**************
1.push data into stack 
2.pop data from the stack
3.displaying the data
4.EXIT
4
imvizag@administrator-ThinkCentre-M82:~/datastructures$ */


